#include <iostream>
#include <ctime>

using namespace std;

int	main()
{
	const int	N = 4;
	int			Array[N][N];
	int			ArrSumm = 0;
	struct tm	date;
	time_t		now = time(nullptr);
	int			day;

	localtime_s(&date, &now);

	day = date.tm_mday;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			cout << (Array[i][j] = { i + j }) << " ";
			ArrSumm = Array[i][j] + ArrSumm;
		}
		if (i == day % N)
			cout << ArrSumm;
		ArrSumm = 0;
		cout << "\n";
 	}
}